package com.example.comeback_is_real;

import retrofit2.Call;
import retrofit2.http.GET;

public interface interface_zapros {
    @GET("api/system/address")
    Call<Addresses> getData();
}
