package com.example.comeback_is_real;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Addresses {
    @SerializedName("addresses")
    List<OneItem> addresses;
}
